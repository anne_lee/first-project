const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentsSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    required: true
  },
  postId: {
    type: String,
    required: true,
  },
  userId: {
    // type: String,
    type: Schema.Types.ObjectId, ref: 'users',
    required: true,
  },
  comment: {
    type: String,
    required: true
  },
});
module.exports = mongoose.model('comments', commentsSchema);
