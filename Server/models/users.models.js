const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username:{
    type: String,
    unique: true,
    required: true
  },
	email: {
    type: String,
    required: true,
    unique: true
  },
  encryptedPassword: {
      type: String,
      required: true
  },
  admin: {
      type: Boolean,
      default: false
  }
});
module.exports = mongoose.model('users', userSchema);
