const mongoose = require('mongoose');

const postsSchema = new mongoose.Schema({
  createdAt:{
    type: Date,
    required: true
  },
  location: { 
    type: Object,
    required: true
  },
	title: {
    type: String,
    required: true
  },
  userId: {
    type: String,
    required: true,
  },
  userName: {
    type: String,
    required: true,
  },
  //even they did not add a media, there would be an empty array
  video: {
    type: Array,
    required: true
  },
  image: {
    type: Array,
    required: true
  }
});
module.exports = mongoose.model('posts', postsSchema);

