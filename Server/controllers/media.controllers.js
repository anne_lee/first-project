const multer = require('multer')
const cloudinary = require('cloudinary')

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
})

const storage = multer.diskStorage({
  destination: 'api',
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '.mov')
  }
})

const upload = multer({ storage: storage }).single('video-upload')
const upload_video = async (req, res) => {
  try {
    upload(req, res, async function (err) {
      console.log("2.2: ", `${process.cwd()}/api/${req.file.filename}`)
      console.log(__dirname, process.cwd())
      cloudinary.v2.uploader.upload(`${process.cwd()}/api/${req.file.filename}`, {
        resource_type: 'video'
      }, function (error, result) {
        error
          ? res.status(500).send({ ok: false, error })
          : res.status(200).send({
            ok: true,
            result: {
              public_id: result.public_id,
              secure_url: result.secure_url,
              type: result.resource_type,
            }
          })
      })
    });
  } catch (error) {
    res.status(500).send({ ok: false, error })
  }
}

module.exports = {
  upload_video
}