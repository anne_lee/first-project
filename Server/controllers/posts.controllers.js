const Posts = require('../models/posts.models');
const jwt = require('jsonwebtoken');
const jwt_secret = process.env.JWT_SECRET;

const createpost = async (req, res) => {
  const { location, title, image, video } = req.body;
  if ( !title || !location ) return res.json({ ok: false, message: 'Title and location is required' });
  try{
    const token = req.headers.authorization;
    console.log(token, jwt_secret)
    jwt.verify(token, jwt_secret, async (err, succ) => {
      console.log(succ)
      if (err) return res.send ({ok:false, err})
    console.log(title, location)
      const added = await Posts.create({
        createdAt: new Date(),
        location: location,
        title: title,
        image: image,
        video: video,
        userId: succ._id,
        userName: succ.username
    })
    res.send({ok: true, added: added})
  })
  } catch (error) {
    console.log('error=====>',error)
    res.send({ok: false, error: error})
  }
}


const editpost =  (req, res) => {
  const { newItems, postId } = req.body;
  console.log('newItems===>', newItems)
  console.log('postId===>',postId)
  const token = req.headers.authorization;
  if ( !newItems ) return res.json({ ok: false, message: 'Title and location is required'}) 
  console.log(newItems)
  try{
    jwt.verify(token, jwt_secret, async (err, succ) => {
      if (err) return res.send ({ok:false, err})

      const post = await Posts.findOne({userId: succ._id});
      if( !post )return res.send({ok:false, message: "cannot update this post"})

      const editted = await Posts.updateOne(
        {_id: postId}, {...newItems}
      )
      res.send({ok: true})
  })
  } catch (error) {
    console.log('error===>', error)
    res.send({ok: false, error: error})
  }
}


const deletepost =  (req, res) => {
  const{ postId } = req.body;
  console.log('postId===>',postId)
	const token = req.headers.authorization;
	try{
		jwt.verify(token, jwt_secret, async (err, succ) => {
		if (err) return res.send ({ok:false, err})

		const removed = await Posts.deleteOne({
      _id: postId
    })
    console.log('removed===>', removed)
		res.json({ok: true})
	})
	}catch (error) {
		console.log('error====>', error)
		res.send({ok: false, error})
	}
}

const findpost = async (req, res) => {
  const {key, value} = req.body
  try {
    const post = await Posts.find({
      [key]: value
    })
    res.json({ok: true, post: post})
  } catch (error) {
    res.send({ok: false, error: error})
  }
} 

const singlepost = async (req, res) => {
  const { postId } = req.params
  console.log('postId=====>', postId)
  try {
    const onePost = await Posts.findOne({
      _id: postId
    })
    res.json({ok: true, onePost: onePost})
  } catch (error) {
    res.send({ok: false, error: error})
  }
}

const allposts = async (req, res) => {
  try {
    const allPosts = await Posts.find({})
    res.status(200).send({ok: true, allPosts: allPosts})
  } catch (error) {
    res.send({ok: false, error: error})
  }
}


const deleteimage = async (req, res) => {
  const { postId, public_id, type } = req.body
  console.log("========",postId, public_id, type)
  try {
    await Posts.updateOne(
    { _id: postId },
    { $pull: { [type]: { public_id: public_id } } }
  )
  res.send({ok: true})
}catch (error) {
    console.log(error)
    res.send({ok: false, error: error})
  }
}

const finduser = async (req, res) => {
  const token = req.headers.authorization;
	try {
		jwt.verify(token, jwt_secret, async (err, succ) => {
			console.log(succ);
			if (err) return res.send({ ok: false, err });
			const foundUser = await Posts.find({
				userId: succ._id
      });
			res.json({ ok: true, foundUser: foundUser });
		});
	} catch (error) {
    console.log('===========>',error)
		res.send({ ok: false, error: error });
	}
};



module.exports = {
  createpost,
  editpost,
  deletepost,
  findpost,
  singlepost,
  allposts,
  deleteimage,
  finduser
}
