const Comments = require('../models/comments.models');
const jwt = require('jsonwebtoken');
const jwt_secret = process.env.JWT_SECRET;


const addcomment = (req, res) => {
  const { postId, comment } = req.body;
  const token = req.headers.authorization;
  if (!comment) return res.json({ ok: false, message: 'Comment is required' });
  try {
    jwt.verify(token, jwt_secret, async (err, succ) => {
      if (err) return res.send({ ok: false, err })
      console.log("comment, postId======>", comment, postId, succ)
      const added = await Comments.create({
        createdAt: new Date(),
        comment: comment,
        userId: succ._id,
        postId: postId
      })
      console.log('added=======>', added)
      res.send({ ok: true })
    })
  } catch (error) {
    console.log('error===>', error)
    res.send({ ok: false, error })
  }
}

// const editcomment = (req, res) => {
//   const { commentId, edittedComment } = req.body;
//   console.log('commentId=====>', commentId)
//   console.log('edittedComment=====>', edittedComment)

//   const token = req.headers.authorization;
//   if ( !edittedComment ) return res.json({ok: false, message: 'Comment is required'})
//   try{
//     jwt.verify(token, jwt_secret, async (err, succ) => {
//       if (err) return res.send ({ok: false, err})

//       const comment = await Comments.findOne ({userId: succ._id});
//       if ( !comment ) return res.send ({ok: false, message: "You cannot edit this comment"})

//       const editted = await Comments.updateOne(
//         {_id: commentId}, {comment: edittedComment, createdAt: new Date()}
//       )
//       console.log('editted=====>', editted)
//       res.send({ok: true, editted: editted})
//     })
//   } catch (error) {
//     console.log('error====>', error)
//     res.send({ok: false, error: error})
//   }
// }

const deletecomment = (req, res) => {
  const { commentId } = req.body;
  const token = req.headers.authorization;
  try {
    jwt.verify(token, jwt_secret, async (err, succ) => {
      if (err) return res.send({ ok: false, err })

      const comment = await Comments.findOne({ userId: succ._id });
      if (!comment) return res.send({ ok: false, message: "You cannot delete this comment" })

      const removed = await Comments.deleteOne({
        _id: commentId
      })
      res.send({ ok: true, message: removed })
    })
  } catch (error) {
    console.log('error===>', error)
    res.send({ ok: false, error })
  }
}

const findcomments = async (req, res) => {
  const { postId } = req.params;
  try {
    const comments = await Comments.find({ postId: postId }).populate("userId")
    res.send({ ok: true, comments: comments })
    // console.log('comments=======>', comments)
  } catch (error) {
    console.log('error=====>', error)
    res.send({ ok: false, error: error })
  }
}

const allcomments = async (req, res) => {
  try {
    const allComments = await Comments.find()
    res.send({ ok: true, allComments: allComments })
    // console.log('allComments=====>', allComments)
  } catch (error) {
    res.send({ ok: false, error: error })
  }
}



module.exports = {
  addcomment,
  // editcomment,
  deletecomment,
  findcomments,
  allcomments
}