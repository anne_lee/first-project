const User = require('../models/users.models');
const argon2 = require('argon2'); //https://github.com/ranisalt/node-argon2/wiki/Options
const jwt = require('jsonwebtoken');
const validator = require('validator');
const jwt_secret = process.env.JWT_SECRET;
// the client is sending this body object
//  {
//     email: form.email,
//     password: form.password,
//     password2: form.password2
//  }
const register = async (req, res) => {
	const { username, email, password, password2 } = req.body;
	if (!username || !email || !password || !password2) return res.json({ ok: false, message: 'All fields are required' });
	if (password !== password2) return res.json({ ok: false, message: 'Passwords must match' });
	if (!validator.isEmail(email)) return res.json({ ok: false, message: 'Please provide a valid email' });
	try {
		// const user = await User.findOne( { $or: [ { email: email }, { username: username } ] } );
		// if (user) return res.json({ ok: false, message: 'Email or username is already in use' });
		const hash = await argon2.hash(password);
		console.log('hash ==>', hash);
		const newUser = {
			username: username,
			email,
			encryptedPassword: hash,
		};
		const userToSign = await User.create(newUser);
		const token = jwt.sign(userToSign.toJSON(), jwt_secret, { expiresIn: '365d' }); 
		res.json({ ok: true, message: 'Successfully registered', token });
	} catch (error) {
		console.log('error===>', error)
		res.json({ ok: false, error });
	}
};

const login = async (req, res) => {
	const {email, password} = req.body; 
	if (!email || !password) return res.json({ ok: false, message: 'All fields are required' });
	if (!validator.isEmail(email)) return res.json({ ok: false, message: 'invalid data provided' });
	try {
		const user = await User.findOne({ email });
		if (!user) return res.json({ ok: false, message: 'invalid data provided' });
		const match = await argon2.verify(user.encryptedPassword, password);
		if (match) {
			const token = jwt.sign(user.toJSON(), jwt_secret, { expiresIn: '365d' }); 
			res.json({ ok: true, message: 'Welcome Back', token, email });
		} else return res.json({ ok: false, message: 'invalid data provided' });
	} catch (error) {
		res.json({ ok: false, error });
	}
};

const verify_token = (req, res) => {
	console.log(req.headers.authorization);
	const token = req.headers.authorization;
	jwt.verify(token, jwt_secret, (err, succ) => {
		err ? res.json({ ok: false, message: 'something went wrong' }) : res.json({ ok: true, succ });
	});
};

const remove =  async (req, res) => {
	const token = req.headers.authorization;
	try{
		jwt.verify(token, jwt_secret, (err, succ) => {
		if (err) return res.send ({ok:false, err})
		const removed =  User.deleteOne({
			userId: succ._id
		})
		res.json({ok: true, message: removed})
	})
	}catch (error) {
		console.log('error====>', error)
		res.send({ok: false, error})
	}
}


module.exports = { 
  register,
  login,
  verify_token,
	remove
}
