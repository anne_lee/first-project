const nodemailer = require('nodemailer')
const transport = nodemailer.createTransport({
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: process.env.NODEMAILER_EMAIL,
		pass: process.env.NODEMAILER_PASSWORD,
	}
});

const send_email = async (req,res) => {
	  const { name , email, subject , message } = req.body
	  const default_subject = "New report"  
	  const mailOptions = {
	  	// to: field is the destination for this outgoing email, your admin email for example
		    to: process.env.NODEMAILER_EMAIL,
		    subject: subject || default_subject, 
		    html: '<p><pre>' + message + '</pre></p>', 
				replyTo: email
	   }
		 console.log(mailOptions)
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({ok:true,message:'email sent'})
      }
      catch( err ){
				console.log(err)
           return res.json({ok:false,message:err})
      }
}

module.exports = { send_email }