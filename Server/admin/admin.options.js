const AdminBro = require('admin-bro');
const AdminBroMongoose = require('admin-bro-mongoose');
const Users = require('../models/users.models')
const Posts = require('../models/posts.models')
const Comments = require('../models/comments.models')

AdminBro.registerAdapter(AdminBroMongoose);

const options = {
	resources: [ Users, Posts, Comments ] // the models must be included in this array
};

module.exports = options;
