const express = require('express');
const app = express();
const AdminBro = require('admin-bro');
const cors = require('cors')
const mongoose = require('mongoose');
require('dotenv').config();
const port = process.env.PORT || 4000
const options = require('./admin/admin.options');
const buildAdminRouter = require('./admin/admin.router');
const run = async () => {
	try {
        await mongoose.connect(process.env.MONGO, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        });
	} catch (error) {
		console.log('error =========>', error);
	}
};
mongoose.set('useCreateIndex', true);
run();

//* ============   ADMIN BRO   ================
const admin = new AdminBro(options);
const router = buildAdminRouter(admin);
app.use(admin.options.rootPath, router);

//* ==============  PARSER   ==================
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//* =============   CORS   ==================
app.use(cors())

//* =============   ROUTES   ==================
app.use('/users', require('./routes/users.routes'))
app.use('/posts', require('./routes/posts.routes'))
app.use('/comments', require('./routes/comments.routes'))
app.use('/emails', require('./routes/email.routes'))
app.use('/payment', require('./routes/payment.route'))
app.use('/media', require('./routes/media.routes'))

//for deployment
const path = require('path');
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../Client/build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../Client/build', 'index.html'));
});

app.listen(port, () => console.log(`🚀 Server running on port ${port} 🚀`));
