const router = require('express').Router();
const controller = require('../controllers/posts.controllers');

router.post('/createpost', controller.createpost);
router.put('/editpost', controller.editpost);
router.post('/deletepost', controller.deletepost);
router.get('/findpost', controller.findpost);
router.get('/singlepost/:postId', controller.singlepost);
router.get('/allposts', controller.allposts)
router.post('/deleteimage', controller.deleteimage)
router.get('/finduser', controller.finduser)

module.exports = router;
