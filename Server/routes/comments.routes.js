const router = require('express').Router();
const controller = require('../controllers/comments.controllers');

router.post('/addcomment', controller.addcomment)
// router.post('/editcomment', controller.editcomment)
router.post('/deletecomment', controller.deletecomment)
router.get('/findcomments/:postId', controller.findcomments)
router.get('/allcomments', controller.allcomments)

module.exports = router;
