const router = require("express").Router();
const controller = require("../controllers/media.controllers.js");

router.post("/upload_video", controller.upload_video);

module.exports = router;