import React, { useState, useEffect } from 'react'
import './App.css'
import Home from './pages/Home'
import { useSetRecoilState } from 'recoil';
import { useRecoilValue } from 'recoil'
import Navbar from './components/Navbar'
import SinglePost from './pages/SinglePost'
import CreatePost from './pages/CreatePost'
import EditAndDeletePost from './pages/EditAndDeletePost'
import PostList from './pages/PostList'
import Contact from './pages/Contact'
import Stripe from "./components/stripe";
import PaymentSuccess from "./containers/payment_success";
import PaymentError from "./containers/payment_error";
import { postDB, commentDB, userDB } from './utils/state';
import { BrowserRouter as Router, Route } from "react-router-dom"
import { url } from './utils/url';
import axios from 'axios';


const App = () => {
  const setPostDBState = useSetRecoilState(postDB)
  const setUserDBState = useSetRecoilState(userDB)
  const setCommentDBState = useSetRecoilState(commentDB)
  const posts = useRecoilValue(postDB)
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  
  const token = JSON.parse(localStorage.getItem('token'));
  
  const verify_token = async () => {
    if (token === null) return setIsLoggedIn(false);
    try {
      axios.defaults.headers.common['Authorization'] = token;
      const response = await axios.post(`${url}/users/verify_token`);
      return response.data.ok ? (setIsLoggedIn(true), setUserDBState(response.data.succ.username)) : setIsLoggedIn(false);
    } catch (error) {
      console.log(error);
    }
  };

  const login = (token) => {
    localStorage.setItem('token', JSON.stringify(token));
    setIsLoggedIn(true);
  };

  const logout = (history) => {
    localStorage.removeItem('token');
    setIsLoggedIn(false);
    history.push("/");
  };

  const getPosts = async () => {
    try {
      const posts = await axios.get(`${url}/posts/allposts`);
      setPostDBState(posts.data.allPosts)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    verify_token();
  }, []);

  useEffect(()=>{
    getPosts()
  },[posts])

  const getComments = async () => {
    try {
      const comments = await axios.get(`${url}/comments/allcomments`)
      setCommentDBState(comments.data.allComments)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    getComments()
  }, [])

  return (
    <div>

      <Router>
        <Navbar logout={logout} isLoggedIn={isLoggedIn}  /*isOpen={isOpen}*/ login={login} />

        <Route exact path="/" component={Home} />
        <Route exact path="/singlepost/:postId" render={props => <SinglePost {...props} getComments={() => getComments} />} />
        <Route exact path="/createpost" component={CreatePost} />
        <Route exact path="/editanddeletepost/:postId" component={EditAndDeletePost} />
        <Route exact path="/postlist" component={PostList} />
        <Route exact path="/contact" component={Contact} />
        {/* <Route exact path="/setting" component={Setting} /> */}
        <Route exact path="/checkout" render={props => <Stripe {...props} />} />
        <Route
          exact
          path="/payment/success"
          render={props => <PaymentSuccess {...props} />}
        />
        <Route
          exact
          path="/payment/error"
          render={props => <PaymentError {...props} />}
        />
      </Router>
    </div>
  );
}

export default App;
