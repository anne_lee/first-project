import { atom } from 'recoil';

export const postDB = atom({
    key: 'postDB',
    default: [
    // {
    //   title: "Car crash with injuries",
    //   location: "Madrid",
    //   createdAt: dateNow,
    //   userName: "aaa",
    //   content: [
    //     {
    //       contentType: "image",
    //       content: "./images/caraccident.jpg"
    //     },
    //     {
    //       contentType: "image",
    //       content: "./images/car.jpg"
    //     },
    //     {
    //       contentType: "image",
    //       content: "./images/carac.jpg"
    //     },
    //     {
    //       contentType: "image",
    //       content: "./images/carac.jpg"
    //     },
    //     {
    //       contentType: "image",
    //       content: "./images/carac.jpg"
    //     }],
    // }, 
    // {
    //   title: "Fire detected at somewhere",
    //   location: "Barcelona",
    //   createdAt: dateNow,
    //   userName: "aaa",
    //   content: [{
    //     contentType: "image",
    //     content: "./images/fire.jpg"
    //   }]
    // },
    // {
    //   title: "Residential balcony collapse",
    //   location: "Valencia",
    //   createdAt: dateNow,
    //   userName: "aaa",
    //   content: [{
    //     contentType: "image",
    //     content: "./images/buildingcollapse.jpg"
    //   },
    //   {
    //     contentType: "video",
    //     content: "https://www.videvo.net/video/fire-background-loop-2/3534/"
    //   }]
    // },
    // {
    //   title: "Something happened at somewhere",
    //   location: "Somewhere",
    //   createdAt: dateNow,
    //   userName: "aaa",
    //   content: [{
    //     contentType: "image",
    //     content: ""
    //   }]
    // },
    // {
    //   title: "Another incident happened",
    //   location: "Somewhere else",
    //   createdAt: dateNow,
    //   userName: "aaa",
    //   content: [{
    //     contentType: "image",
    //     content: "./images/accident.jpg"
    //   }]
    // },
  ]
});



export const commentDB = atom({
  key: 'commentDB',
  default: [
  // {
  // createdAt: dateNow,
  // postId: "1234",
  // userId: "aaa",
  // comment: "the creater of this content added an update "
  // },
  // {
  //   createdAt: dateNow,
  //   postId: "5678",
  //   userId: "bbb",
  //   comment: "this is a comment from some user"
  // },
  // {
  //   createdAt: dateNow,
  //   postId: "9012",
  //   userId: "ccc",
  //   comment: "comment comment comment comment"
  // },
  // {
  //   createdAt: dateNow,
  //   postId: "3456",
  //   userId: "ddd",
  //   comment: "some more comments comments"
  // }
]})

export const userDB = atom({
  key: 'userDB',
  default: ""
})
