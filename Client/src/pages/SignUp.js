import React, {useState} from 'react'
import axios from 'axios'
import {url} from '../utils/url'
import { withRouter } from 'react-router-dom'

const SignUp =(props)=> {
	
  const [ form, setValues ] = useState({
    username: '',
		email: '',
		password: '',
		password2: ''
	});
	const [ message, setMessage ] = useState('');

	const handleChange = (e) => {
		setValues({ ...form, [e.target.name]: e.target.value });
	};
	
	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const response = await axios.post(`${url}/users/register`, {
        username: form.username,
				email: form.email,
				password: form.password,
				password2: form.password2
			});
			setMessage(response.data.message);
			if (response.data.ok) {
				setTimeout(() => {
					props.history.push('/');
					props.setOpenSignup(false)
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};

	const handleSwitch =()=>{
		props.setOpenSignup(false)
		props.setOpenLogin(true)
	}

  return <div className="popup-box">
    <div className="box">
      <span className="close-icon" onClick={()=>props.setOpenSignup(false)}>x</span>

     <div className="sign-up">
      <h1>Sign Up</h1>

      <form onChange={handleChange} onSubmit={handleSubmit}>
        <h2>{message}</h2>
        <div><input name="username" placeholder="Username"/></div>
        <div><input name="email" placeholder="Email address"/></div>
        <div><input type="password" name="password" placeholder="Password"/></div>
        <div><input type="password" name="password2" placeholder="Confirm password"/></div>
        <div><button >Sign Up</button></div>
      </form>

      <h1>----------- or -----------</h1>

      <h3>If you have an account</h3>
			<button onClick={handleSwitch}>Log In</button>
			

    </div>

    </div>
  </div>
}

export default withRouter(SignUp)