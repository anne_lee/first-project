import React, {useState} from 'react'
import axios from 'axios'
import {url} from '../utils/url'
import { withRouter } from 'react-router-dom'

const LogIn  =(props)=> {

  const [ form, setValues ] = useState({
		email: '',
		password: '',
	});
	const [ message, setMessage ] = useState('');
	const [ signedUp, setSignedUp ] = useState(true)

  const handleChange = (e) => {
		setValues({ ...form, [e.target.name]: e.target.value });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const response = await axios.post(`${url}/users/login`, {
				email: form.email,
				password: form.password,
			});
			if (response.data.ok) {
				setMessage(response.data.message);
				setTimeout(() => {
					props.history.push('/')
					props.setOpenLogin(false)
          props.login(response.data.token)
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};

	const handleSwitch =()=>{
		props.setOpenSignup(true)
		props.setOpenLogin(false)
	}

  return <div className="popup-box">
    <div className="box">
      <span className="close-icon" onClick={()=>props.setOpenLogin(false)}>x</span>

     <div className="sign-up">
      <h1>Log In</h1>
      <h2>{message}</h2>
      <form onChange={handleChange} onSubmit={handleSubmit}>
      <div><input name="email" placeholder="Email address"/></div>
        <div><input type="password" name="password" placeholder="Password"/></div>
        <div><button>Log In</button></div>
      </form>

      <h1>----------- or -----------</h1>

      <h3>If you do not have an account</h3>
			<button onClick={handleSwitch}>Sign Up</button>

    </div>

    </div>
  </div>
}

export default withRouter(LogIn)