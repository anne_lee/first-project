import React, { useState } from 'react'
import AutoComp from '../components/AutoComp'
import UploadImages from '../components/UploadImages'
import axios from 'axios'
import {url} from '../utils/url'
import { withRouter } from "react-router-dom";

const CreatePost =(props)=> {

  const [title, setTitle] = useState("")
  const [location, setLocation] = useState(null)
  const [video, setVideo] = useState([])
  const [image, setImage] = useState([])
  const [message, setMessage] = useState("")

  const handleSubmit =async (e)=> {
    e.preventDefault()
    if (!title || location == null ) return alert ("Title and location are required")
    try{
      const token = JSON.parse(localStorage.getItem('token'));
      axios.defaults.headers.common['Authorization'] = token;
      const response = await axios.post(`${url}/posts/createpost`, {title, location, image, video, })
      if (response.data.ok) {
				setMessage("Post successfully created")
				setTimeout(
					() => {
						setMessage("")
            props.history.push("/")
					}, 2000)
			} else {
				 alert("try again")
      }
    }catch(error){
      console.log(error)
    }
  }

  return <form onSubmit={handleSubmit} className="margin">
    <h3>Enter Title </h3>
    <div><input className="input-title" onChange={(e)=>setTitle(e.target.value)} placeholder="Enter title of what happened"/></div>

    <h3>Enter Location</h3>
    <AutoComp setLocation={setLocation}/>

    <h3>Add Image</h3>
    <UploadImages list={image} method={setImage} type={"image"} className="widget-button" />
    {image.map(e=> 
      <img src={e.secure_url} height="120px" width="150px" ></img>
    )}

    <h3>Add Video</h3>
    <UploadImages list={video} method={setVideo} type={"video"} className="widget-button"/>
    {video.map(e=> 
      <iframe src={e.secure_url} height="120px" width="150px" title="Iframe Example"></iframe>
    )}

    <div className="posting-position">{message}</div>
    <div className="posting-position"><button className="posting">Create a Post</button></div>

  </form>
}

export default withRouter(CreatePost)

