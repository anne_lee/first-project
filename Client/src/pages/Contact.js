import axios from 'axios'
import React, { useState } from 'react'
import {url} from '../utils/url'

const Contact =()=> {

  const [ name, setName ] = useState("")
  const [ email, setEmail ] = useState("")
  const [ subject, setSubject] = useState("")
  const [ message, setMessage ] = useState("")

  const handleSubmit= async(e)=> {
    e.preventDefault()
    if (!name || !email || !subject || !message) return alert ("All fields are required")
    try{
      const token = JSON.parse(localStorage.getItem('token'));
      axios.defaults.headers.common['Authorization'] = token;
      const response = await axios.post(`${url}/emails/send_email`,{name, email, subject, message})
      if (response.data.ok) alert ("Your message has been sent")
    } catch (error) {
      console.log(error)
    }
  }

  return <div className="margin contact">
  <form onSubmit={handleSubmit} >

    <p>Name</p>
    <input onChange={(e)=>setName(e.target.value)} type="text"/> 

    <p>Email</p>
    <input onChange={(e)=>setEmail(e.target.value)} type="text"/> 

    <p>Subject</p>
    <input onChange={(e)=>setSubject(e.target.value)} type="text"/> 

    <p>Message</p>
    <textarea type="text" className="contact-message"  onChange={(e)=>setMessage(e.target.value)} /> 

    <button>Send</button>
  </form>

  </div>
}

export default Contact