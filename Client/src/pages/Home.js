import React, { useState, useEffect } from 'react'
import Timeline from '../components/Timeline'
import Map from '../components/Map'
import { useRecoilValue } from 'recoil'
import { postDB } from '../utils/state'

const Home = () => {

  const [status, setStatus] = useState(null);
  const [ loc, setLoc ] = useState({
    lat: null,
    lng: null
  })

  const getLocation = () => {
    if (!navigator.geolocation) {
      setStatus('Geolocation is not supported by your browser');
    } else {
      setStatus('Locating...');
      navigator.geolocation.getCurrentPosition((position) => {
        setStatus(null);
        setLoc({
        lat: position.coords.latitude,
        lng: position.coords.longitude
        })
      }, () => {
        setStatus('Unable to retrieve your location');
      });
    }
  }

  const postDBState = useRecoilValue(postDB) 

  useEffect(() => {
    getLocation()
  }, [])

return <div>

<div className="home">
    <div className="home-map">
      <Map postDBState={postDBState} center={loc}/>
    </div>

    <div>
      <Timeline />
    </div>
 </div>
</div>
}

export default Home

