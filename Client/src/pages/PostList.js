import React, { useEffect, useState } from 'react'
import { NavLink } from "react-router-dom";
import { url } from '../utils/url';
import Map from '../components/Map'
import moment from 'moment'
import axios from 'axios';

const PostList = (props) => {

	const token = JSON.parse(localStorage.getItem('token'));
  axios.defaults.headers.common['Authorization'] = token;

  const [ userPost, setUserPost ] = useState([])

  const getPosts = async() => {
    try {
      const response = await axios.get(`${url}/posts/finduser`);
      setUserPost(response.data.foundUser)
    } catch(error) {
      console.log(error)
    }
  }

	useEffect(() => {
    getPosts();
	}, []);


  const listPost = () => {
    return userPost.map(e => <div>
       <NavLink className="edit-delete" exact to={`/editanddeletepost/${e._id}`}>Edit/Delete</NavLink>

      <div className="postlist-whole" onClick={()=>props.history.push(`/SinglePost/${e._id}`)}>
        <div className="postlist-thumbnail">
          {e.image.length > 0 && <img height="150px" width="180px" src={e.image[0].secure_url} />}
          {e.image.length === 0 && e.video.length > 0 && <iframe src={e.video[0].secure_url} height="150px" width="180px" title="video"></iframe>}
          {e.image.length === 0 && e.video.length === 0 && <div className="postlist-map"><Map height="150px" width="180px" center={e.location} /></div>}
        </div>

        <div className="postlist-detail">
          <h5>{moment(e.createdAt).format('MMMM D h:mma')}</h5>
          <h2>{e.title}</h2>
        </div>
      </div>
    </div>)
  }

  return <div>
    <div className="postlist-grid">{listPost()}</div>
  </div>
}

export default PostList