import React, { useState, useEffect } from 'react'
import { postDB, commentDB } from '../utils/state';
import { useRecoilValue } from 'recoil'
import { useSetRecoilState } from 'recoil';
import moment from 'moment'
import Map from '../components/Map'
import axios from 'axios';
import { url } from '../utils/url'

const Singlepost = (props) => {

  const postDBState = useRecoilValue(postDB)
  // const commentDBState = useRecoilValue(commentDB)
  // const setCommentDBState = useSetRecoilState(commentDB)

  const [singlePost, setSinglePost] = useState({
    title: "",
    location: {},
    image: [],
    video: []
  })
  const [comment, setComment] = useState("")
  const [message, setMessage] = useState("")
  const [commentsForThePost, setCommentsForThePost] = useState([])
  const [userOfTheComment, setUserOfTheComment] = useState("")

  const findPost = () => {
    const id = props.match.params.postId
    const result = postDBState.find(e => e._id === id)
    setSinglePost(result)
  }
  useEffect(() => {
    postDBState.length !== 0 && findPost()
  }, [postDBState]);



  const findComments = async () => {
    try {
      const postId = props.match.params.postId
      const response = await axios.get(`${url}/comments/findcomments/${postId}`)
      setCommentsForThePost(response.data.comments)
      setUserOfTheComment(response.data.comments)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    findComments()
  }, [])

  const handleChange = (e) => {
    const data = e.target.value
    setComment(data)
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    if (!comment) return alert("Enter a comment")
    try {
      const token = JSON.parse(localStorage.getItem('token'));
      axios.defaults.headers.common['Authorization'] = token;
      const response = await axios.post(`${url}/comments/addcomment`, { comment, postId: props.match.params.postId })
      if (response.data.ok) {
        setMessage("successfully commented")
        setTimeout(
          () => {
            setMessage("")
          }, 2000)
        findComments()
      } else {
        alert("Sign up to comment")
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handleDelete = async (e) => {
    e.preventDefault()
    try {
     const token = JSON.parse(localStorage.getItem('token'));
     axios.defaults.headers.common['Authorization'] = token;
     const response = await axios.post(`${url}/comments/deletecomment`, {commentId: commentsForThePost._id})
     if(response.data.ok) {
       setMessage("Comment successfully deleted")
       setTimeout(
         ()=> {
          setMessage("")
         }, 2000)
         } else {
           alert ("try again")
         }
        }catch (error) {
          console.log (error)
        }
  }

  const handleSend =async (e)=> {
    e.preventDefault()
    try {
      const message = `There was a report for Post: ${props.match.params.postId}`
      const response = await axios.post(`${url}/emails/send_email`,{message})
      if (response.data.ok) alert ("Your report has been sent")
    } catch (error) {
      console.log(error)
    }
  }
     
  return <div>
    <div className="singlepost-header">
      <div className="singlepost-map"><Map center={singlePost.location} /></div>
      <div>
        <h1>{singlePost.title}</h1>
        <h3>{moment(singlePost.createdAt).format('MMMM Do YYYY, h:mma')}</h3>
      </div>
    </div>
     

    <div className="singlepost-content">
        {singlePost.image.map(e =>
          <img className="" src={e.secure_url} />
        )}
        {singlePost.video.map(e =>
          <iframe src={e.secure_url}  title="video"></iframe>)}
    </div>

    <div className="created-by">
        <p>content created by: {singlePost.userName}</p>
        <button onClick={(e)=>handleSend(e)}>Report this post</button>
    </div>

    <div>
    {commentsForThePost.map((e, i) => <div key={i}>
        <p>{e.userId.username} commented</p>
        <h2>{e.comment}</h2>
        <p>{moment(e.createdAt).format('MMMM Do h:mma')}</p>
        {/* <button onClick={handleDelete}>Delete</button> */}
        <hr/>
      </div>)}

      {message}
      <form onSubmit={handleSubmit} >
        <input className="addcomment-input" placeholder="Add comment" onChange={handleChange} />
        <button className="addcomment-button">comment</button>
      </form>
    </div>

  </div>
}

export default Singlepost
