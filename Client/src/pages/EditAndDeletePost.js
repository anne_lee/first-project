import React, { useState, useEffect } from 'react';
import AutoComp from '../components/AutoComp';
import UploadImages from '../components/UploadImages';
import axios from 'axios';
import { url } from '../utils/url';
import { postDB } from '../utils/state';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { withRouter } from "react-router-dom";


const EditAndDeletePost = (props) => {

	const [ location, setLocation ] = useState({});
	const [ video, setVideo ] = useState([]);
	const [ image, setImage ] = useState([]);
	const [ tempVideo, setTempVideo ] = useState([]);
	const [ tempImage, setTempImage ] = useState([]);
	const [ message, setMessage ] = useState("");
	const postDBState = useRecoilValue(postDB);
	const setPostDBState = useSetRecoilState(postDB);
	const [ oldPost, setOldPost ] = useState({
		title: '',
		location: {},
		image: [],
		video: []
	});

	useEffect(
		() => {
			if (postDBState.length !== 0) {
				const post = postDBState.find((e) => e._id === props.match.params.postId);
				setOldPost(post);
				setLocation(post.location);
				setVideo(post.video);
				setImage(post.image);
			}
		},
		[ postDBState ]
	);

	useEffect(
		() => {
			setTimeout(
				() => {
					setMessage ("")
				}, 2000)
		}, [message]
	)

	const handleEdit = async (e) => {
		e.preventDefault();
		if (!oldPost.title || location == null) return alert('Title and location are required');
		try {
			if (tempImage.length + image.length > 5) return alert();
			if (tempVideo.length + video.length > 5) return alert();
			const token = JSON.parse(localStorage.getItem('token'));
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.put(`${url}/posts/editpost`, {
				newItems: {
					title: oldPost.title,
					location,
					image: [ ...image, ...tempImage ],
					video: [ ...video, ...tempVideo ]
				},
				postId: oldPost._id
			});
			if (response.data.ok) {
				setMessage("Change is saved")
				setTimeout(
					() => {
						props.history.push("/")
					}, 2000)
				} else {
				 alert("try again")
				}
		} catch (error) {
			console.log(error);
		}
	};

	const handleDelete = async (e) => {
		e.preventDefault();
		try {
			const token = JSON.parse(localStorage.getItem('token'));
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.post(`${url}/posts/deletepost`, { postId: oldPost._id });
			if (response.data.ok) {
			setMessage("Post successfully deleted")
			setTimeout(
				() => {
					props.history.push("/")
				}, 2000)
			} else {
			 alert("try again")
			}
			} catch (error) {
			console.log(error);
		}
	};
	
	const handleDeleteTempImage = (public_id, type) => {
		if (type === 'image') {
			const temp = [ ...tempImage ];
			const index = temp.findIndex((el) => el.public_id === public_id);
			temp.splice(index, 1);
			setTempImage(temp);
			setMessage("Image successfully deleted")
			setTimeout(
				() => {
					
				}, 2000)
		} else if ( type === 'video') {
			const temp = [ ...tempImage ];
			const index = temp.findIndex((el) => el.public_id === public_id);
			temp.splice(index, 1);
			setTempImage(temp);
			setMessage("Video successfully deleted")
			setTimeout(
				() => {
					
				}, 2000)
		}
	};
  
	const handleDeleteImage = async (public_id, type) => {
		try {
			const response = await axios.post(`${url}/posts/deleteimage`, { postId: oldPost._id, public_id, type });
			if (!response.data.ok) return alert("Try again");
			const temp = Object.assign({}, oldPost);
			const index = temp[type].findIndex((el) => el.public_id === public_id); //index of the image or the video in the old post
			const tempElem = [ ...temp[type] ];
			tempElem.splice(index, 1); //erase it inside the old post
			temp[type] = tempElem;
			const oldIndex = postDBState.findIndex((el) => el._id === oldPost._id); //finding the index of the post inside state
			const temp2 = [ ...postDBState ]; //making the copy of the state
			temp2[oldIndex] = temp; //replacing the post with the updated one
			setPostDBState(temp2); // setting the state
			if (response.data.ok) {
				setMessage("Image successfully deleted")
				setTimeout(
					() => {						
					}, 2000)
			} else {
				 alert("try again")
			}
		} catch (error) {
			console.log(error);
		}
	};
	return (
		<div className="margin">
			<form onSubmit={handleEdit}>
				<h3>Enter Title </h3>
				<div>
					<input className="input-title"
						onChange={(e) => setOldPost({ ...oldPost, title: e.target.value })}
						value={oldPost.title}
					/>
				</div>
				<h3>Enter Location</h3>
				<AutoComp setLocation={setLocation} />
				<h3>Add Images</h3>
				{image.map((e) => (
					<div>
						<button type="button" onClick={() => handleDeleteImage(e.public_id, 'image')}>
							delete
						</button>
						<img src={e.secure_url} height="120px" width="150px" />
					</div>
				))}
				{tempImage.map((e) => (
					<div>
						<button type="button" onClick={() => handleDeleteTempImage(e.public_id, 'image')}>
							delete
						</button>
						<img src={e.secure_url} height="120px" width="150px" />
					</div>
				))}
				<UploadImages
					list={tempImage}
					method={setTempImage}
					len={tempImage.length + image.length}
					type={'image'}
					className="widget-button"
				/>
				<h3>Add Video</h3>
				{video.map((e) => (
					<div>
						<button type="button" onClick={() => handleDeleteImage(e.public_id, 'video')}>
							delete
						</button>
						<iframe src={e.secure_url} height="120px" width="150px" />
					</div>
				))}
        <UploadImages list={video} method={setVideo} len={tempImage.length + image.length} type={'video'} className="widget-button"/>

				<div className="posting-position">
					{message}
				</div>

				<div className="posting-position">
					<button className="posting">Edit Post</button>
				</div>
			</form>

			<div className="posting-position">
				<button className="delete" onClick={handleDelete}>Delete Post</button>
			</div>
		</div>
	);
};
export default withRouter(EditAndDeletePost);