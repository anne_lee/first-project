import React, { useState } from 'react'
import SignUp from '../pages/SignUp'
import LogIn from '../pages/LogIn'
import { NavLink, withRouter } from "react-router-dom";

const Navbar = (props) => {

  const [openSignup, setOpenSignup] = useState(false)
  const [openLogin, setOpenLogin] = useState(false)


  return <div className="nav">
    <div>
      <NavLink exact to={"/"}><div className="iReport"><h1>iReport</h1></div></NavLink>
    </div>

    <div>
      {props.isLoggedIn
        ? <div>
          <NavLink exact to={"/CreatePost"}>Create a new post</NavLink>｜
          <NavLink exact to={"/PostList"}>List of your posts</NavLink>｜
          <NavLink exact to={"/Contact"}>Contact</NavLink>｜
          <NavLink exact to={"/checkout"}>Donation</NavLink>｜
          <input className="login-logout" type="button" value="Log out" onClick={() => props.logout(props.history)} />
        </div>
        : <div>
          <NavLink exact to={"/Contact"}>Contact</NavLink>｜
          <NavLink exact to={"/checkout"}>Donation</NavLink>｜
          <input className="login-logout" type="button" value="Sign up/Log in" onClick={() => setOpenSignup(true)} />

          {openSignup && <SignUp
            openSignup={openSignup}
            setOpenSignup={(bool) => setOpenSignup(bool)}
            openLogin={openLogin}
            setOpenLogin={(bool) => setOpenLogin(bool)}
            
            login={props.login} />}

          {openLogin && <LogIn
            openSignup={openSignup}
            setOpenSignup={(bool) => setOpenSignup(bool)}
            openLogin={openLogin}
            setOpenLogin={(bool) => setOpenLogin(bool)}
            login={props.login}
          />}


        </div>
      }
    </div>
  </div>
}

export default withRouter(Navbar)
