import React from 'react'
import widgetStyles from '../widgetStyles';

const UploadImages = props => {

	const uploadWidget = () => {
		window.cloudinary.openUploadWidget({
			cloud_name: process.env.REACT_APP_CLOUD_NAME,
			upload_preset: process.env.REACT_APP_UPLOAD_PRESET,
			tags: ['user'],
			stylesheet: widgetStyles
		}, (error, result) => {
			if (error) {
			} else {
				if (result[0].resource_type === props.type) {
					const temp = [...props.list]
					const data = {
						secure_url: result[0].secure_url,
						public_id: result[0].public_id
					}
				temp.push(data)
				props.method(temp)
				} else {
					alert ("you should upload the correct type of media")
				}
			}
		});
	}

	//↓saving image directly to the server
	// const upload_picture = async (result) => {
	//       try{ 
	// 		const response = await axios.post('http://localhost:5050/pictures/upload',{	
	// 																			photo_url:result[0].secure_url, 
	// 																			public_id:result[0].public_id
	// 																		})
	// 		response.data.ok 
	// 		? await props.fetch_pictures()
	// 		: alert('Something went wrong')
	// 	}catch(error){
	// 		debugger
	// 	}
	// }

	return (
		<div className="flex_upload">
			<div className="upload">
				<button className="button" disabled={props.len > 4} type="button"
					onClick={uploadWidget} > open widget
				</button>
			</div>
		</div>
	)
}

export default UploadImages
