import React, {useState, useEffect} from 'react'
import { useRecoilValue } from 'recoil'
import { postDB } from '../utils/state';
import moment from 'moment'
import Map from '../components/Map'
import { withRouter } from "react-router-dom";

const Timeline =(props)=> {

  const [ search, setSearch ] = useState("")
  const post = useRecoilValue(postDB)

  const handleChange = (e) => {
    setSearch(e.target.value)
  }

  useEffect(()=>{
    
  }, [post])


  const eachpost = post?.slice(0).reverse().filter(e=>{
    if(e.title.toLowerCase().includes(search.toLowerCase())) return true
    else {return false}
  }).map(e=>{
    
    return <div>
      <div className="each-post" onClick={()=>props.history.push(`/SinglePost/${e._id}`)}>
      <div>
        {e.image.length > 0 && <img height="75px" width="100px" src={e.image[0].secure_url} />}
        {e.image.length === 0 && e.video.length > 0 && <iframe src={e.video[0].secure_url} height="75px" width="100px" title="video"></iframe>}
        {e.image.length === 0 && e.video.length === 0 && <div className="time-line-map"><Map center={e.location} /></div>}
      </div>

      <div>
        <h1>{e.title}</h1>
        <h2>{moment(e.createdAt).format('MMMM D, h:mma')}</h2>
      </div>
    </div>
    </div>})
  
  

  return <div className="timeline">
      <input onChange={handleChange} placeholder="search" className="search"/>
 
    {eachpost}

  </div>
}

export default withRouter(Timeline)