import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';
import { useRecoilValue } from 'recoil'
import { postDB } from '../utils/state'
import moment from 'moment'
import { withRouter } from "react-router-dom";

const Map = (props) => {

  const [location, setLocation] = useState({
    lat: 0,
    lng: 0
  })
  const [isShown, setIsShown] = useState(false);

  const postDBState = useRecoilValue(postDB)

  useEffect(() => {
    if (props.center) {
      setLocation(props.center)
    }
  }, [props.center])

  return <GoogleMapReact
    bootstrapURLKeys={{ key: '' }}
    center={props.center}
    defaultZoom={props.zoom}
  >

    {props.postDBState
      ? props.postDBState.map(el =>
        <Marker 
        lat={el.location.lat} lng={el.location.lng} 
        metaData="cartop" 
        setIsShown={(id) =>setIsShown (id)}
        isShown={isShown}
        id={el._id}
        title={el.title}
        createdAt={el.createdAt}
        history={props.history}
        />
        )
      : <Marker lat={location.lat} lng={location.lng} metaData="cartop" />}
    
  </GoogleMapReact>
}


const Marker = (props) => {

  return <div>
  <div style={{ position: 'absolute', transform: 'translate(-50%, -50%)' }}

  onMouseEnter={() => props.id !== undefined && props.setIsShown(props.id)}
  onMouseLeave={() => props.id !== undefined && props.setIsShown(null)} 
  >
    {props.metaData === 'cartop' && (
      <img onClick={()=>props.history.push(`/SinglePost/${props.id}`)}
      style={ props.isShown === props.id ? { width: '60px' } : { width: '35px' } }
      src={'https://secure.webtoolhub.com/static/resources/icons/set94/ca5fd179.png'}
      />
    )}
  </div>
  { props.isShown === props.id 
    && <div className="text-box">
      <h1>{props.title}</h1>
      {moment(props.createdAt).format('M/D h:mma')}
      </div>}
  </div>
};

export default withRouter(Map)
Map.defaultProps = {
  zoom: 12
}