import React from "react";

const Product = ({ name, images, quantity, amount }) => {
  return (
    <div className="product_container">
      <div className="image_container">
        <img alt="product_image" className="image" src={images[0]} />
      </div>
      <div className="text_container">
        <p>product : {name}</p>
      </div>
      <div className="text_container">
        <p>quantity : {quantity}</p>
      </div>
      <div className="text_container">
        <p>price : {amount} €</p>
      </div>
      <div className="text_container">
        <p>total price : {amount * quantity} €</p>
      </div>
    </div>
  );
};

export default Product;
