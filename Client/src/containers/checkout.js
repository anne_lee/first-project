import React, { useState } from "react";
import axios from "axios";
import { injectStripe } from "react-stripe-elements";

const Checkout = props => {
  
  const createCheckoutSession = async () => {
    try {
      const response = await axios.post(
        "http://localhost:4000/payment/create-checkout-session",
        { products }
      );
      // debugger
      return response.data.ok
        ? (localStorage.setItem(
            "sessionId",
            JSON.stringify(response.data.sessionId)
          ),
          redirect(response.data.sessionId))
        : props.history.push("/payment/error");
    } catch (error) {
      props.history.push("/payment/error");
    }
  };
  
  const redirect = sessionId => {
    debugger;
    props.stripe
      .redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      })
      .then(function(result) {
        // debugger;
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
  };
  
const [products, setProducts] = useState([
  {
    name: "donation",
  quantity: 1,
  images: [],
  amount: 10
}
])

const increase =(inc)=> {
  const temp = [...products]
  temp[0].amount = inc ? temp[0].amount + 1 : temp[0].amount -1
  setProducts(temp)
}

return <div className="margin">
      <span>donation of €</span>
      <span><input type="button" className="donation-amount" value="-" onClick={()=>products[0].amount > 0 && increase(false)}/></span>

      <span>{products[0].amount}</span>
      
      <span><input type="button" className="donation-amount" value="+" onClick={()=>increase(true)} /></span>
    <div>
      <input type="button" value="make donation" className="donation-button" onClick={()=>createCheckoutSession()}/>
    </div>
</div>
}

export default injectStripe(Checkout);


